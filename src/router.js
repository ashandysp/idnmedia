import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Homepage.vue'
import CardDetail from './views/CardDetail.vue'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/card-detail',
      name: 'card-detail',
      component: CardDetail
    }
  ]
})
