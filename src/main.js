import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import 'bootstrap'

Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')

window.onscroll = function() {
  var d = document.documentElement //IE with doctype
  if(d.scrollTop >= 201) {
		if(document.getElementById('floating-subnav').classList.contains("visible") == false) {
			document.getElementById('floating-subnav').classList.add("visible")
		}
	} else if(d.scrollTop <= 200) {
		if(document.getElementById('floating-subnav').classList.contains("visible")) {
			document.getElementById('floating-subnav').classList.remove("visible")
		}
  }
}

window.onclick = function(e) {
	var btnCategory = document.getElementById('btn-category')
	var navCategory = document.getElementById('nav-category')
	if(btnCategory != e.target) {
		if(navCategory.classList.contains('show')) {
			navCategory.classList.remove('show')
		}
	}
}
